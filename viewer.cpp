/*
 * Copyright (C) 2019-2020 Rafael Diniz <rafael@riseup.net>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 *
 */

#include <unistd.h>
#include <cstdint>
#include <sstream>
#include <iostream>
#include <memory>
#include <thread>

#include <Open3D.h>

#include "metric.h"
#include "pc_utils.h"

#include "ColorSpace/ColorSpace.h"
#include "ColorSpace/Comparison.h"

using namespace open3d;
using namespace std;

int main(int argc, char *argv[])
{
    bool print_neighborhood = false;
    unsigned long int point_nr = 0;
    auto pc = make_shared<geometry::PointCloud>();
    auto vis = std::make_shared<visualization::Visualizer>();

    if (argc < 3)
    {
    usage_info:
        fprintf(stderr, "Usage: %s [-n xx] input.ply output.png\n", argv[0]);
        return EXIT_FAILURE;
    }

    if (io::ReadPointCloud(argv[argc-2], *pc)) {
        fprintf(stderr, "Successfully read %s\n", argv[argc-2]);
    } else {
        fprintf(stderr, "Failed to file %s.\n",argv[argc-2]);
        return EXIT_FAILURE;
    }

    int opt;
    while ((opt = getopt(argc, argv, "n:")) != -1){
        switch (opt){
        case 'n':
            print_neighborhood = true;
            point_nr = atol(optarg);
            fprintf(stderr, "Printing 8-neighborhood of point %d.\n", point_nr);
            break;
        default:
            fprintf(stderr, "Wrong command line.\n");
            goto usage_info;
        }
    }

    
//    pc->EstimateNormals(geometry::KDTreeSearchParamHybrid(0.1, 30)); // radius, max_nn
//    pc->NormalizeNormals();

//    vis->CreateVisualizerWindow("GPDS PC Viewer", 1280, 960, 50, 50, false);



    vis->CreateVisualizerWindow("GPDS PC Viewer", 1280, 960, 50, 50, true);

//    int neighborhood_size = 8;
    int neighborhood_size = 12;
//    double voxel_size = 0.002;

    if (print_neighborhood)
    {
        geometry::KDTreeFlann kdtree;

//        pc = pc->VoxelDownSample(voxel_size);
        kdtree.SetGeometry(*pc);

        auto pc_n = make_shared<geometry::PointCloud>();

        if (point_nr >= pc->points_.size())
        {
            fprintf(stderr, "Error: point_nr >= pc->points_.size()\n");
            exit(EXIT_FAILURE);
        }

        std::vector<int> indices_vec(neighborhood_size+1);
        std::vector<double> dists_vec(neighborhood_size+1);

        // kdtree.SearchKNN(pc->points_[point_nr], neighborhood_size+1, indices_vec, dists_vec);
        kdtree.SearchKNN(pc->points_[point_nr], neighborhood_size+1, indices_vec, dists_vec);

        const Eigen::Vector3d &point_color = pc->colors_[point_nr];
        const Eigen::Vector3d &point_normal = pc->normals_[point_nr];

        for (int i = 0; i < neighborhood_size+1; i++)
        {
            Eigen::Vector3d coord = pc->points_[indices_vec[i]];
            Eigen::Vector3d color = pc->colors_[indices_vec[i]];
            Eigen::Vector3d normal = pc->normals_[indices_vec[i]];

            double diff = 0;

            if (i == 0)
            { // paint red
                color(0) = 1;
                color(1) = 0;
                color(2) = 0;
                pc_n->points_.push_back(coord);
                pc_n->colors_.push_back(color);
                pc_n->normals_.push_back(normal);
                fprintf(stderr, "added %f %f %f, %f %f %f, %f %f %f\n", coord(0), coord(1), coord(2),  color(0) * 255, color(1) * 255, color(2) * 255, normal(0), normal(1), normal(2));
            }
            else
            {
                pc_n->points_.push_back(coord);
                pc_n->colors_.push_back(color);
                pc_n->normals_.push_back(normal);
                // fprintf(stderr, "added %f %f %f, %f %f %f, %f %f %f\n", coord(0), coord(1), coord(2), color(0) * 255, color(1) * 255, color(2) * 255, normal(0), normal(1), normal(2));

                ColorSpace::Rgb a(point_color(0) * 255, point_color(1) * 255, point_color(2) * 255);
                ColorSpace::Rgb b(color(0) * 255, color(1) * 255, color(2) * 255);
                // CIE LAB Delta E 2000 (CIEDE2000)
                diff = ColorSpace::Cie2000Comparison::Compare(&a, &b);
                fprintf(stderr, "CIEDE2000: %f\n", diff);

                diff =  sqrt ( (pow((point_normal[0] - normal[0]), 2)) +
                               (pow((point_normal[1] - normal[1]), 2)) +
                               (pow((point_normal[2] - normal[2]), 2)) );
                // fprintf(stderr, "NORMALDST: %f\n", diff);

            }
        }
        pc = pc_n;
    }

//    auto voxel_pc = geometry::VoxelGrid::CreateFromPointCloud(*pc, voxel_size);
//    auto voxel_pc = pc;
//     RenderOption *render = vis->GetRenderOption();
    vis->AddGeometry({pc});
//    vis->AddGeometry({voxel_pc});
    while (vis)
    {
        vis->UpdateGeometry();
        vis->PollEvents();
        vis->UpdateRender();
    }
    vis->CaptureScreenImage(argv[2], true);

    return EXIT_SUCCESS;
}
