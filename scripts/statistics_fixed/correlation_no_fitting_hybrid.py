#!/usr/bin/python3

import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from scipy.stats import spearmanr, pearsonr
from sklearn.metrics import mean_squared_error

# df = pd.read_csv('data.csv')
df = pd.read_csv(sys.argv[1])

#result = df.dtypes
#print(result)

# df["distancia"] = df["distancia"].astype(float)

X = df["distancia"]
y = df["MOS"]

df["distancia"] = df["distancia"].replace(np.inf, 99)

#X[X == np.inf] = 99
#y[y == np.inf] = 99

#X = X.values.reshape(-1, 1)

#X = df["distancia"].values.reshape(-1, 1)
#y = df["MOS"].values.reshape(-1, 1)

#print("Distance")
#print(df["distancia"])
#print("MOS")
#print(df["MOS"])

# print(pol_reg.coef_)

corr, p_value = pearsonr(df["distancia"], df["MOS"])

#print("PCC,"+str(corr))
print(str(corr), end = '')

corr, p_value = spearmanr(df["distancia"], df["MOS"])

#print("SROCC,"+str(corr))
print(","+str(corr), end = '')

print(","+str(mean_squared_error(df["distancia"], df["MOS"], None, squared=False)))
