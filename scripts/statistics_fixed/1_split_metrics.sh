#!/bin/bash

for metric in $(seq 0 10); 
do
  for neighbors in $(seq 6 12); 
  do
    for database in apsipa19 qomex19 spie19;
    do
      DIR=1_RAW_FEATURES_PER_METRIC/metric-${metric}/neighbors-${neighbors}/${database}/
      mkdir -p ${DIR}
      
      for k in k0.7 k1.0 k1.5 k2.0 k3.0 k4.0 k5.0 k6.0 novox
      do
        input=0_INPUT_RAW_FEAURES/${database}/results-${k}.csv
        output=${DIR}/features-${k}.csv
        cat ${input} | grep "metric_${metric}," | grep n_${neighbors} > ${output}

        # Add header
        columns=$(head -1 ${output} | sed 's/[^,]//g' | wc -c )
        features=$((columns - 3))
        header="file,metric,neighbors"
        for col in $(seq 1 ${features})
        do
          header+=",fv${col}"
        done
        echo ${header} | cat - ${output} > temp && mv temp ${output}
        echo ${output}
      done
    done
  done
done