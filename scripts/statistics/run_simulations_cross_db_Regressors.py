import os
import pandas as pd
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import pearsonr, spearmanr, kendalltau
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import ARDRegression, BayesianRidge
from sklearn.linear_model import Lars, ElasticNet, ElasticNetCV, Lasso
from sklearn.linear_model import RANSACRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)



def mean_squared_error(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())


models = [
    RandomForestRegressor(
        n_estimators=1000, criterion='mse', max_depth=None, min_samples_split=2,
        min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
        max_leaf_nodes=None, min_impurity_decrease=0.0,
        min_impurity_split=None, bootstrap=True, oob_score=False, n_jobs=None,
        random_state=None, verbose=0, warm_start=False
    ),
    ExtraTreesRegressor(
        n_estimators=1000, criterion='mse', max_depth=None, min_samples_split=2,
        min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
        max_leaf_nodes=None, min_impurity_decrease=0.0, 
        min_impurity_split=None, bootstrap=True, oob_score=False, n_jobs=None,
        random_state=None, verbose=0, warm_start=False
    ),
    GradientBoostingRegressor(
        loss='huber', learning_rate=0.1, n_estimators=1000, subsample=1.0,
        criterion='friedman_mse', min_samples_split=2, min_samples_leaf=1,
        min_weight_fraction_leaf=0.0, max_depth=3, min_impurity_decrease=0.0,
        min_impurity_split=None, init=None, random_state=None,
        max_features=None, alpha=0.9, verbose=0, max_leaf_nodes=None,
        warm_start=False, validation_fraction=0.1,
        n_iter_no_change=None, tol=0.0001
    ),
    BayesianRidge(
        n_iter=300, tol=0.001, alpha_1=1e-06, alpha_2=1e-06, lambda_1=1e-06,
        lambda_2=1e-06, compute_score=False,
        fit_intercept=True, normalize=False, copy_X=True, verbose=False
    ),
    ARDRegression(
        n_iter=300, tol=0.001, alpha_1=1e-06, alpha_2=1e-06, lambda_1=1e-06,
        lambda_2=1e-06, compute_score=False, threshold_lambda=10000.0,
        fit_intercept=True, normalize=False, copy_X=True, verbose=False
    ),
    Lars(),
    ElasticNet(),
    ElasticNetCV(cv=5),
    Lasso(),
    RANSACRegressor(),
     KNeighborsRegressor(
         n_neighbors=5, weights='uniform', algorithm='auto', leaf_size=30,
         p=3, metric='minkowski', metric_params=None, n_jobs=None
    ),
    MLPRegressor(
        hidden_layer_sizes=(2, ), activation='logistic', solver='lbfgs',
        alpha=0.0001, batch_size='auto', learning_rate='adaptive',
        learning_rate_init=0.001, power_t=0.5, max_iter=20000, shuffle=True,
        random_state=None, tol=0.0001, verbose=False, warm_start=False,
        momentum=0.9, nesterovs_momentum=True, early_stopping=False,
        validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08,
        n_iter_no_change=10
    )
]


def run(ref_pattern, test_pattern):
    ref_filepath = "distances/results-{p}.csv".format(p=ref_pattern)
    test_filepath = "distances/results-{p}.csv".format(p=test_pattern)
    df_ref = pd.read_csv(ref_filepath)
    df_test = pd.read_csv(test_filepath)
    simulations = len(df_test.index)
    output = []

    x_train, y_train = df_ref.distancia_raw.values, df_ref.subjective_MOS.values
    x_train = x_train.reshape(-1, 1)
    y_train = y_train.ravel()

    for model in models:
        modelname = type(model).__name__
        model.fit(x_train, y_train)
        for i in range(simulations):
            row = df_test.iloc[[i]]
            x_test, y_test = row.distancia_raw.values, row.subjective_MOS.values
            x_test = x_test.reshape(-1, 1)
            y_pred = model.predict(x_test)
            residual = y_test - y_pred

            line = {
                'simulation': i,
                'nome_do_pc': row["nome_do_pc"].tolist()[0],
                'nome_referencia': row["nome_referencia"].tolist()[0],
                'distancia_raw': x_test[0],
                'MOS': y_test[0],
                'MOSp': y_pred[0],
                'residual': residual[0],
            }
            output.append(line)
        df_out = pd.DataFrame.from_dict(output)
        outpattern = "{r}_{t}".format(r=ref_pattern, t=test_pattern)
        mkdir("simulations_{m}_cross".format(m=modelname))
        output_filename = "simulations_{m}_cross/results-{p}.csv".format(
            m=modelname, p=outpattern)
        df_out.to_csv(output_filename)

        rmse = mean_squared_error(df_out.MOSp, df_out.MOS)
        SROCC = spearmanr(df_out.MOSp, df_out.MOS)[0]
        PCC = pearsonr(df_out.MOSp, df_out.MOS)[0]
        print(modelname, outpattern, "PCC=", PCC, "SROCC=", SROCC, "RMSE=", rmse)


if __name__ == "__main__":
    for ref_db in ['D1', 'D2', 'D3']:
        for test_db in ['D1', 'D2', 'D3']:
            for strategy in ['S1', 'S2']:
                ref = "{d}-{s}".format(d=ref_db, s=strategy)
                test = "{d}-{s}".format(d=test_db, s=strategy)
                if ref != test:
                    run(ref, test)
