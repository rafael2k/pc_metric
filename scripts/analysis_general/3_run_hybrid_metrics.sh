#!/bin/sh

# our library
. ./pc_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

PC_METRIC_BIN=${PREFIX}/bin/pc_metric_journal
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram_journal_split
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size

MOSP_BIN=/home/rafael2k/pc_metric/scripts/statistics_fixed/correlation_no_fitting.py
#MOSP_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py
DISTANCE_BIN=/home/rafael2k/pc_metric/scripts/statistics_fixed/script_compute_distances.py
SINGLE_DB_LOGISTIC=/home/rafael2k/pc_metric/scripts/statistics_fixed/script_simulation_singledb_logistic.py
SINGLE_DB_REGRESSORS=/home/rafael2k/pc_metric/scripts/statistics_fixed/script_simulation_singledb_Regressors.py
LINEPLOT_CORRELATION=/home/rafael2k/pc_metric/scripts/statistics_fixed/script_lineplot_correlation_per_k.py
LINEPLOT_CORRELATION_HYBRID=/home/rafael2k/pc_metric/scripts/statistics_fixed/script_plot_hybrid.py

CORRELATE_HYBRID=/home/rafael2k/pc_metric/scripts/statistics_fixed/correlation_no_fitting_hybrid.py

VOXEL_INFO=voxelization.csv


# this is score output basename
SCORE_OUTPUT=raw_scores

# for python pre-processing step
PY_TEMP_DATA=data-temp.csv
PY_TEMP_DATA2=data-temp2.csv
PY_CORRELATION_OUTPUT=correlation.csv


# scores in csv format
D1_MOS=/mnt/ssd/RafaelDiniz/queiroz/scores.csv
D2_MOS=/mnt/ssd/RafaelDiniz/QoMEX_2019/scores/scores.csv
D3_MOS=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/scores/scores.csv
D4_MOS=/mnt/ssd/RafaelDiniz/icip2020pc/AllLabsResultsSummary-noref.csv
D5_MOS=/mnt/ssd/RafaelDiniz/sjtu-PCQA/mos-finalized.csv

D1_DATAPATH=/mnt/ssd/RafaelDiniz/queiroz/with_normals
D2_DATAPATH=/mnt/ssd/RafaelDiniz/QoMEX_2019/with_normals
D3_DATAPATH=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/with_normals
D4_DATAPATH=/mnt/ssd/RafaelDiniz/icip2020pc/with_normals
D5_DATAPATH=/mnt/ssd/RafaelDiniz/sjtu-PCQA/with_normals

D1_RESULTS=/mnt/ssd/RafaelDiniz/queiroz/results-our-metrics
D2_RESULTS=/mnt/ssd/RafaelDiniz/QoMEX_2019/results-our-metrics
D3_RESULTS=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc/results-our-metrics
D4_RESULTS=/mnt/ssd/RafaelDiniz/icip2020pc/results-our-metrics
D5_RESULTS=/mnt/ssd/RafaelDiniz/sjtu-PCQA/results-our-metrics


#switches to enable or disable each metric
M0=1
M1=1
M2=1
M3=1
M4=1
M5=1
M6=1
M7=1
M8=1
M9=1
M10=1
M11=1
M12=1
M13=1
M14=1
M15=1

METRICS_LIST="0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15"

NEIGHBOURHOOD_LIST=6,7,8,9,10,11,12

NEIGHBOURHOOD_LIST_W_SPACES="6 7 8 9 10 11 12"

VOXEL_SIZES="0.7 1.0 1.3 1.6 2.0 3.0 4.5 6.0 7.5"


# data-sets...
# for i in 1 2 3 4; do
for i in 1 3 4 5; do


  DATA_PATH=$( eval "echo \$D${i}_DATAPATH" )
  CSV=$( eval "echo \$D${i}_MOS" )

  OUTPUT_DIR=$( eval "echo \$D${i}_RESULTS" )

  OUTPUT_DIR_SDB_LOGISTIC="${OUTPUT_DIR}/sdb-logistic"
  OUTPUT_DIR_SDB_REGRESSORS="${OUTPUT_DIR}/sdb-regressors"

  OUTPUT_DIR_HYBRID_DIR="${OUTPUT_DIR}/geotex-geo_m11_n6_novox-color_m1"



  hybrid_geotex_raw
##  hybrid_geotex

  logistic_correlation_hybrid


  plot_logistic_hybrid
##  correlate_no_fitting_hybrid



done
