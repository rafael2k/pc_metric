#!/bin/sh

## update to use: https://github.com/AlirezaJav/Point_to_distribution_metric

# our library
. ./pc_functions.sh

# PREFIX=/usr
PREFIX=/home/rafael2k

CSV=/mnt/ssd/RafaelDiniz/VALID/VALID_PC_iterative.csv

PC_METRIC_BIN=${PREFIX}/bin/pc_metric
CORRELATE_BIN=${PREFIX}/bin/correlate
COMPARE_HISTOGRAM_BIN=${PREFIX}/bin/compare_histogram
VOXELIZE_BIN=${PREFIX}/bin/optimize_voxel_size
MOSP_BIN=/home/rafael2k/pc_metric/scripts/predicted_mos.py

MPEG_PCC_BIN=/home/rafael2k/metrics/mpeg-pcc-dmetric/test/pc_error
PC_CONVERT_BIN=${PREFIX}/bin/pc_convert
MATLAB_BIN=/mnt/hd/RafaelDiniz/MATLAB/R2018a/bin/matlab

CORRELATION_OUTPUT=correlation.csv
SCORE_OUTPUT=scores.csv

OUTPUT_DIR=/mnt/ssd/RafaelDiniz/VALID/results

# for python pre-processing step
PY_OUTPUT_DATA=data.csv
PY_MOS_INPUT=scores-mean.txt
PY_CORRELATION_OUTPUT=correlation-py.csv

DATA_PATH=/mnt/ssd/RafaelDiniz/VALID/converted_db
NORMALS_DIR=/mnt/ssd/RafaelDiniz/VALID/with_normals

DATA_PATH_XYZ=/mnt/ssd/RafaelDiniz/VALID/format_xyz

MPEG_SCORE_OUTPUT=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-scores.txt

PTPOINT_MSE=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptpointmse.txt
PTPOINT_MSE_PSNR=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptpointmsepsnr.txt

PTPOINT_H=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptpointh.txt
PTPOINT_H_PSNR=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptpointhpsnr.txt

PTPLANE_MSE=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptplanemse.txt
PTPLANE_MSE_PSNR=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptplanemsepsnr.txt

PTPLANE_H=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptplaneh.txt
PTPLANE_H_PSNR=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ptplanehpsnr.txt

COLOR_TEMP=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-temp.txt

COLOR_MSE_Y=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ymse.txt
COLOR_PSNR=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-ypsnr.txt
COLOR_H_Y=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-yh.txt
COLOR_H_PSNR=/mnt/ssd/RafaelDiniz/results/mpeg/mpeg-hpsnr.txt

ANGULAR_SCRIPT_TEMP=angular_script.m
ANGULAR_SCORE_OUTPUT_MSE=angular_scores-mse.csv
ANGULAR_SCORE_OUTPUT_RMS=angular_scores-rms.csv

mkdir -p ${OUTPUT_DIR}

#create_normals
#mpeg_pcc_metrics

#parse_mpeg_metrics

#angular_similarity


correlate_mpeg_python


