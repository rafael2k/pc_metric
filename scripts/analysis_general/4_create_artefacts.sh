#!/bin/bash

# our bash library
. pc_functions.sh

ENABLE_MPEG=1
ENABLE_OUR_METRICS=0
ENABLE_HYBRID=1

NEIGHBOURHOOD_LIST_W_SPACES="6 7 8 9 10 11 12"
VOXEL_SIZES="0.7 1.0 1.3 1.6 2.0 3.0 4.5 6.0 7.5"


D1_RESULTS=/mnt/ssd/RafaelDiniz/queiroz
D2_RESULTS=/mnt/ssd/RafaelDiniz/QoMEX_2019
D3_RESULTS=/mnt/ssd/RafaelDiniz/dataset-quality-assessment-for-pcc
D4_RESULTS=/mnt/ssd/RafaelDiniz/icip2020pc
D5_RESULTS=/mnt/ssd/RafaelDiniz/sjtu-PCQA

COMPILED_RESULTS=/mnt/ssd/RafaelDiniz/PC-results-m11-m1

#datasets list to use
DATASETS="1 3 4 5"
#DATASETS="5"


echo "Cleaning up artifacts directory..."
rm -rf ${COMPILED_RESULTS}/*

# data-sets...
for i in ${DATASETS}; do

  RESULTS_RAW=$( eval "echo \$D${i}_RESULTS" )

#  RESULTS_HYBRID="${RESULTS_RAW}/results-our-metrics/geotex-geo_m11_n6_novox-color_m10"
  RESULTS_HYBRID="${RESULTS_RAW}/results-our-metrics/geotex-geo_m11_n6_novox-color_m1"

  RESULTS_OTHER="${RESULTS_RAW}/other-metrics"

  OUTPUT_DIR="${COMPILED_RESULTS}/D${i}"

  mkdir -p ${OUTPUT_DIR}/hybrid
  mkdir -p ${OUTPUT_DIR}/mpeg


  cp -a ${RESULTS_HYBRID}/* ${OUTPUT_DIR}/hybrid
  cp -a ${RESULTS_OTHER}/* ${OUTPUT_DIR}/mpeg

  parse_mpeg_correlation
  parse_hybrid_correlation

  echo "Check ${OUTPUT_DIR}/hybrid ${OUTPUT_DIR}/mpeg"

done
