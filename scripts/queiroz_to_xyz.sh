#!/bin/sh

DATA_PATH=/mnt/ssd/RafaelDiniz/queiroz
PC_CONVERT_BIN=/usr/bin/pc_convert
OUTPUT_DIR=/mnt/ssd/RafaelDiniz/queiroz-xyz

mkdir -p ${OUTPUT_DIR}

for name in testing training; do
  cd ${DATA_PATH}/${name}

  for i in $(ls -1 *.ply); do
    ${PC_CONVERT_BIN} ${i} ${OUTPUT_DIR}/$(basename ${i} .ply).xyz
  done;
done;
